class Code
  attr_reader :pegs

  PEGS = {
    r: "red", 
    g: "green", 
    b: "blue",
    y: "yellow",
    o: "orange",
    p: "purple"
  }

  def initialize(array_of_pegs)
    @pegs = array_of_pegs
  end

  def self.parse(input)
    result = input.chars.map do |char|
      char = char.downcase.to_sym
      if PEGS.keys.include?(char)
        PEGS[char]
      else 
        raise "invalid selection"
      end
    end
    Code.new(result)
  end

  def self.random
    result = []
    until result.length == 4
      rand_num = rand(0..11)
      result << PEGS.flatten[rand_num] if rand_num.odd?
    end
    Code.new(result)
  end

  def [](idx)
    pegs[idx]
  end

  def exact_matches(code)
    counter = 0
    code.pegs.each_with_index do |color1, idx| 
      counter += 1 if @pegs[idx] == color1
    end
    counter
  end

  def near_matches(code)
    counter = 0
    code.pegs.each_with_index do |color1, idx|
      self_occurances = self.pegs.count(color1)
      code_occurances = code.pegs[(idx..-1)].count(color1)
      counter += 1 if self_occurances >= code_occurances
    end
    counter -= exact_matches(code)
  end

  def ==(code)
    return true if code.instance_of?(Code) && exact_matches(code) == 4
  end

end

class Game
  attr_reader :secret_code

  def initialize(code=Code.random)
    @secret_code = code
  end

  def get_guess
    puts "Enter your guess as 4 letter string. E.g. rgby"
    begin
      Code.parse(gets.chomp)
    rescue
      puts "Invalid color selection!"
      retry
    end
  end

  def display_matches(code)
    puts "exact #{@secret_code.exact_matches(code)}"
    puts "near #{@secret_code.near_matches(code)}"
  end

  def play
    (0...10).each do |idx|
      guess = get_guess
      display_matches(guess)
      if guess == @secret_code
        puts "You Win!"
        return
      else
        puts "Guess Again" if idx < 9
      end
    end
    puts "You lose!"
  end

end

if __FILE__ == $PROGRAM_NAME
  Game.new.play
end